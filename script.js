$( document ).ready(function() {

// Canvas setup
	var canvas = document.getElementById('myCanvas');
	var ctx = canvas.getContext('2d');

	var canvasHeight = 300;
	var canvasWidth = 500;

	var cw = 20;
	var snakeSpeed = 120; // in ms
	var snakeSpeedup = 60; // in ms
	var food;
	var d;
	var score;
	var snake;
	var snakeLenght;

	var scoreDiv = document.getElementById('score');
	var headingDiv = document.getElementById('heading');
	var textDiv = document.getElementById('text');
	document.getElementById("startTheGame").value = "Play now";


// Event listeners
	document.addEventListener('keydown', keyDownHandler, false);
	document.addEventListener('keyup', keyUpHandler, false);

// Function to update the game
	function init(){
		d = 'right';
		createSnake();
		createFood();
		score = 0;

		// jQuery library used to show and hide elements on the website, START OF THE GAME

		$("#logo").attr('src', 'images/logo.png');
		$("#myCanvas").show();
		$("#score").show();
		$("#startTheGame").hide();

		headingDiv.innerHTML = "SNAKE " + "<span>GAME</span>";
		textDiv.innerHTML = "This game has been developed by Mantas Salasevicius!";

		if(typeof timer != "undefined") clearInterval(timer);
		timer = setInterval(paint, snakeSpeed);
	}

	init();

// Function to paint the assets
	function paint(){

		// Lets fill the canvas

		ctx.fillStyle = 'white';
		ctx.fillRect(0, 0, canvasWidth, canvasHeight);
		ctx.strokeStyle = '#dad9d9';
		ctx.strokeRect(0, 0, canvasWidth, canvasHeight);

		var nx = snake[0].x;
		var ny = snake[0].y;

		// Direction based movement

		if(d == "right") {
			nx++;
		} else if(d == "left"){
			nx--;
		} else if(d == "up"){
			ny--;
		} else if(d == "down"){
			ny++;
		}

		// Restart the game

		if(nx == -1 || nx == canvasWidth/cw || ny == -1 || ny == canvasHeight/cw || checkCollision(nx, ny, snake)){

			// jQuery library used to show and hide elements on the website, when GAMEOVER

			$("#myCanvas").hide();
			$("#score").hide();
			$("#startTheGame").show();

			$("#logo").attr('src', 'images/over-snake.png');
			headingDiv.innerHTML = "GAME " + "<span>OVER</span>";
			textDiv.innerHTML = "Your score was  " + "<span>" + score + "</span>";
			document.getElementById("startTheGame").value = "Play again";
			return;

		}
		// Code for the snake to eat the food

		if(nx == food.x && ny == food.y){
			var tail = {x: nx, y: ny};
			score++;
			createFood();
		} else {
			var tail = snake.pop();
			tail.x = nx;
			tail.y = ny;
		}

		snake.unshift(tail);

		for(var i = 0; i < snake.length; i++){
			var c = snake[i];

			paintSnake(c.x, c.y);
		}

		paintFood(food.x, food.y);
		paintScore();
	}


// Function to paint the snake
	function paintSnake(x,y) {
		var img = document.getElementById("snake");
		ctx.drawImage(img, x*cw, y*cw, cw, cw);
	}

// Function to create the snake
	function createSnake(){
		var snakeLenght = 5; // The lenght of the starting
		snake = [];

		// This will create the snake

		for(var i = snakeLenght-1; i>=0; i--) {
			snake.push({x: i, y:0});
		}
	}

// Function to paint the food
	function paintFood(x,y) {
		var img = document.getElementById("apple");
		ctx.drawImage(img, x*cw, y*cw, cw, cw);
	}

// Function to create the food
	function createFood(){
		food = {
			x: Math.round(Math.random()*(canvasWidth-cw)/cw),
			y: Math.round(Math.random()*(canvasHeight-cw)/cw),
		};
	}

// Function to display the score
	function paintScore() {
		scoreDiv.innerHTML = "Your score: " + "<span>" + score + "</span>";
	}

// Function to check collision
	function checkCollision(x, y, array){
		for(var i = 0; i < array.length; i++){
			if(array[i].x == x && array[i].y == y)
			 return true;
		}

		return false;
	}

// When keys are NOT PRESSED
	function keyUpHandler(e){
		if(e.keyCode == 32) {
			if(typeof timer != "undefined") clearInterval(timer);
			timer = setInterval(paint, snakeSpeed);
		} else if(e.keyCode == 16){
			if(typeof timer != "undefined") clearInterval(timer);
			timer = setInterval(paint, snakeSpeed);
		}
	}

// When keys are PRESSED
	function keyDownHandler(e) {
		if(e.keyCode == 39 && d != "left"){  // Right arrow
			d = "right";
		} else if(e.keyCode == 37 && d != "right") { // Left arrow
			d = "left";
		} else if(e.keyCode == 38 && d != "down") { // Up arrow
			d = "up";
		} else if(e.keyCode == 40 && d != "up") { // Down arrow
			d = "down";
		} else if(e.keyCode == 68 && d != "left"){  // D
			d = "right";
		} else if(e.keyCode == 65 && d != "right") { // A
			d = "left";
		} else if(e.keyCode == 87 && d != "down") { // W
			d = "up";
		} else if(e.keyCode == 83 && d != "up") { // S
			d = "down";
		} else if (e.keyCode == 32) {  // Space
			if(typeof timer != "undefined") clearInterval(timer);
			timer = setInterval(paint, snakeSpeedup);
		} else if (e.keyCode == 16) {  // Shift
			if(typeof timer != "undefined") clearInterval(timer);
			timer = setInterval(paint, snakeSpeedup);
		}
	}
});
